#ifndef BLOCK_H
#define BLOCK_H

#include <QString>
#include <QtGlobal> // for Qint64 data type
#include <vector>
#include "transaction.h"
using namespace std;
class Block
{
private:
    QString hash; // current block hash
    QString prevHash; // previes block hash
    int index; // index number of current block
    int proof; // nonce, POW number that solve hashing to start with some this like '0000' at hash start
    vector<Transaction> transactions;

public:
    /*
     * Constructor with parameter
     * Arguments:
        * :index
        * :prevHash
        * :hash
        * :proof (nonce)
    */
    Block(int index, QString prevHash, QString hash , vector<Transaction> &transactions, int proof);
    QString getHash();
    QString toJson();
    QString toString();


    // print block information:
    string printBlock();
};
#endif // BLOCK_H
