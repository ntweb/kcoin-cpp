#include "blockchain.h"
#include <QString>
#include <iostream>
#include <QCryptographicHash>
Blockchain::Blockchain()
{
    // create genesus block
    Block genesus(1,"0","0", this->bendingTransaction, 0);
    this->chain.push_back(genesus);
}

Transaction Blockchain::createNewTransaction(Transaction &tx)
{
    this->bendingTransaction.push_back(tx);
    return tx;
}


void Blockchain::mineBlock(int &proof, QString &prevHash, QString &hash)
{
    Block *block = new Block(this->chain.size()+1,prevHash,hash, this->bendingTransaction, proof);
    this->bendingTransaction.clear();
    this->chain.push_back(*block);
    delete block;
}
Block &Blockchain::getLastBlock()
{
    return this->chain.at(this->chain.size()-1);
}

Block Blockchain::getCurrentBlock()
{
    return  Block(this->chain.size()+1, this->getLastBlock().getHash(), "", this->bendingTransaction, 0);
}

QString Blockchain::hashBlock(QString &prevHash, Block & currentBlock, int &proof)
{
    QString qstr = prevHash + QString::number(proof) + currentBlock.toString();
    QByteArray hash = QCryptographicHash::hash(qstr.toLocal8Bit(), QCryptographicHash::Sha256);
    //cout << qstr.toStdString()<< endl;
    return hash.toHex();
}

int Blockchain::proofOfWork(QString &prevHash, Block &currentBlock)
{
    int proof = 0;
    //Block currentBlock = this->getCurrentBlock();
    QString hash = this->hashBlock(prevHash, currentBlock, proof);
    QString hashStartWith = "00000";
    while (QString::compare(hash.left(5), hashStartWith) != 0)
    {
        proof++;
        hash = this->hashBlock(prevHash, currentBlock, proof);
        //cout << hash.toStdString() << endl;
    }
    cout << proof << endl;
    return proof;
}
