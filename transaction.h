#ifndef TRANSACTION_H
#define TRANSACTION_H

#include <QString>

using namespace std;
class Transaction
{
private:
    double amount;
    QString sender;
    QString recipient;
    QString id;
public:
    Transaction(double amount, QString sender, QString recipient);
    QString toJson();
    QString toString();
};

#endif // TRANSACTION_H
