#ifndef BLOCKCHAIN_H
#define BLOCKCHAIN_H

#include <transaction.h>
#include <block.h>

class Blockchain
{
private :
    vector<Transaction> bendingTransaction;
    vector<Block> chain;
public:
    Blockchain();
    Transaction createNewTransaction(Transaction &tx);
    Block & getLastBlock();
    Block getCurrentBlock();
    QString hashBlock(QString &prevHash, Block &currentBlock, int &proof);
    int proofOfWork(QString &prevHash, Block &currentBlock);
    void mineBlock(int &proof, QString &prevHash, QString &hash);
};

#endif // BLOCKCHAIN_H
