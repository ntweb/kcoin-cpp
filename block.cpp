#include "block.h"
#include <QDateTime>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
//constructor
Block::Block(int index, QString prevHash, QString hash, vector<Transaction> &transactions, int proof) : index(index), prevHash(prevHash), transactions(transactions), hash(hash), proof(proof)
{
    //this->timestamp = QDateTime::currentMSecsSinceEpoch();
}



// print information
string Block::printBlock()
{
    return "";
};

QString Block::getHash()
{
    return this->hash;
}

// convert block to json
QString Block::toJson()
{
    QJsonObject jsonObject;
    jsonObject.insert("index", this->index);
    jsonObject.insert("hash", this->hash);
    jsonObject.insert("prevHash", this->prevHash);
    jsonObject.insert("proof", this->proof);

    QJsonArray txsArray;

    // looping throw all txs and add each one to json array
    for (int i=0; i < this->transactions.size(); i++)
    {
        txsArray.push_back(this->transactions.at(i).toJson());
    }

    jsonObject.insert("transactions", txsArray);
    QJsonDocument json(jsonObject);
    return json.toJson(QJsonDocument::Compact);
}
// convert block to json
QString Block::toString()
{
    QString str = "index=" + QString::number(this->index);
    str+= ", hash=" + this->hash;
    str+= ", prevHash=" + this->prevHash;
    str+= ", proof=" + QString::number(this->proof);
    str+= ", transactions=";



    QString txs="";

    // looping throw all txs and add each one to json array
    for (int i=0; i < this->transactions.size(); i++)
    {
        txs += "{"+ this->transactions.at(i).toString()+"},";
    }

    str += txs;
    return str;
}
