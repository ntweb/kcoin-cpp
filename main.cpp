#include "mainwindow.h"
// #include <QApplication>
#include <iostream>
#include "block.h"
#include "blockchain.h"
#include "transaction.h"
int main(int argc, char *argv[])
{

    Blockchain kcoins;
    for (int i=0; i < 15; i++)
    {
        Transaction tx(i,"nasser","zahra");
        kcoins.createNewTransaction(tx);
        Block currentBlock = kcoins.getCurrentBlock();
        QString prevHash = kcoins.getLastBlock().getHash();
        int proof = kcoins.proofOfWork(prevHash, currentBlock);
        QString hash = kcoins.hashBlock(prevHash,currentBlock,proof);
        kcoins.mineBlock(proof,prevHash,hash);
    }

    /*
    kcoins.createNewTransaction(Transaction(50,"nasser","zahra"));
    Block currentBlock = kcoins.getCurrentBlock();
    QString prevHash = kcoins.getLastBlock().getHash();
    //int proof = kcoins.proofOfWork(prevHash, currentBlock);
    QString hash = kcoins.hashBlock(prevHash,currentBlock,0);
    //kcoins.mineBlock(proof, kcoins.getLastBlock().getHash(), hash);

    //cout << kcoins.hashBlock("test");

    /*
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
    */
}
