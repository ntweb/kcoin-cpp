#include "transaction.h"
#include <QJsonObject>
#include <QJsonDocument>
Transaction::Transaction(double amount, QString sender, QString recipient) : amount(amount), sender(sender), recipient(recipient)
{
    this->id = "0x";
}


// convert transaction to json
QString Transaction::toJson()
{
    QJsonObject jsonObject;
    jsonObject.insert("amount", this->amount);
    jsonObject.insert("sender", this->sender);
    jsonObject.insert("recipient", this->recipient);
    jsonObject.insert("id", this->id);

    QJsonDocument json(jsonObject);
    return json.toJson(QJsonDocument::Compact);
}


// convert transaction to text
QString Transaction::toString()
{
    QString str = "amount=" + QString::number(this->amount);
    str+= ", sender=" + this->sender;
    str+= ", recipient=" + this->recipient;
    str+= ", id=" + this->id;
    return str;
}
